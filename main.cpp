#include <iostream>
#include <iomanip>
#include <list>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

const int companyCount = 9;
const int workerCount = 47;
const int masterWorkerCount = 10;

class Worker {

public:
	string name;
	vector<string> history;

	Worker() {
		name = "W-NaN";
	}

	Worker(string m_name) {
		name = "W-" + m_name;
	}

	void ResetHistory() { history.clear(); }
	void Transfer(string companyName) { history.push_back(companyName); }

	bool RotationDone() {

		if (history.size() == companyCount)
			return true;

		return false;
	}

	bool WorkedHere(string companyName) {

		if (history.empty())
			return false;

		for (string name : history) {
			if (name == companyName)
				return true;
		}

		return false;
	}
};

class WorkerList {

public:
	Worker ptr[workerCount];

	WorkerList() {

		for (int i = 0; i < workerCount; i++) {
			Worker temp(to_string(i + 1));

			ptr[i] = temp;
		}
	}

	bool RotationDone() {

		for (int i = 0; i < workerCount; i++) {

			if (ptr[i].history.size() != companyCount)
				return false;
		}

		cout << "MAKSIMUM ITARASYON DOLDU" << endl;
		return true;
	}

	vector<int> FreeGuys(string companyName, int rotationTurn) {
		vector<int> answer;

		for (int i = 0; i < workerCount; i++) {
			bool workCheck = !ptr[i].WorkedHere(companyName);
			bool rotationCheck = rotationTurn > ptr[i].history.size();

			if (workCheck && rotationCheck)
				answer.push_back(i);
		}

		return answer;
	}

	void EraseHistory() {

		for (int i = 0; i < workerCount; i++) {
			ptr[i].ResetHistory();
		}
	}
};

class MasterWorker {

public:
	string name;
	vector<string> history;

	MasterWorker() {
		name = "LW-NaN";
	}

	MasterWorker(string m_name) {
		name = "LW-" + m_name;
	}

	void ResetHistory() { history.clear(); }
	void Transfer(string companyName) { history.push_back(companyName); }

	bool RotationDone() {

		if (history.size() == companyCount)
			return true;

		return false;
	}

	bool WorkedHere(string companyName) {

		if (history.empty())
			return false;

		for (string name : history) {
			if (name == companyName)
				return true;
		}

		return false;
	}
};

class MasterWorkerList {

public:
	MasterWorker ptr[masterWorkerCount];

	MasterWorkerList() {

		for (int i = 0; i < masterWorkerCount; i++) {
			MasterWorker temp(to_string(i + 1));

			ptr[i] = temp;
		}
	}

	bool RotationDone() {

		for (int i = 0; i < masterWorkerCount; i++) {

			if (ptr[i].history.size() != companyCount)
				return false;
		}

		return true;
	}

	vector<int> FreeGuys(string companyName, int rotationTurn) {
		vector<int> answer;

		for (int i = 0; i < masterWorkerCount; i++) {
			bool workCheck = !ptr[i].WorkedHere(companyName);
			bool rotationCheck = rotationTurn > ptr[i].history.size();

			if (workCheck && rotationCheck)
				answer.push_back(i);
		}

		return answer;
	}

	void EraseHistory() {

		for (int i = 0; i < masterWorkerCount; i++) {
			ptr[i].ResetHistory();
		}
	}
};

class Company {
public:
	string name;
	int workerCount;
	int masterWorkerCount;

	vector <string> workerHistory;
	vector <string> masterWorkerHistory;

	Company() {
		name = "C-NaN";
		workerCount = 0;
		masterWorkerCount = 0;
	}

	Company(string c_name, int w_count, int mw_count) {
		name = c_name;
		workerCount = w_count;
		masterWorkerCount = mw_count;
	}

	void getWorker(string w_name) {
		workerHistory.push_back(w_name);
	}

	void getMasterWorker(string mw_name) {
		masterWorkerHistory.push_back(mw_name);
	}

	void ResetWorkerHistory() { workerHistory.clear(); }
	void ResetMasterWorkerHistory() { masterWorkerHistory.clear(); }
};

class CompanyList {

public:

	Company ptr[companyCount];

	CompanyList() {

		Company temp0("A", 4, 1);
		Company temp1("B", 8, 1);
		Company temp2("C", 8, 2);
		Company temp3("D", 6, 2);
		Company temp4("E", 3, 1);
		Company temp5("F", 2, 1);
		Company temp6("G", 5, 1);
		Company temp7("H", 1, 0);
		Company temp8("J", 1, 0);

		ptr[0] = temp0;
		ptr[1] = temp1;
		ptr[2] = temp2;
		ptr[3] = temp3;
		ptr[4] = temp4;
		ptr[5] = temp5;
		ptr[6] = temp6;
		ptr[7] = temp7;
		ptr[8] = temp8;
	}

	bool RotationDone() {
		int size;

		for (int i = 0; i < companyCount; i++) {

			size = workerCount - ptr[i].workerHistory.size();

			if (ptr[i].workerCount > size) {
				cout << "MAKSIMUM ITARASYON DOLDU";
				cout << endl << "************************************************************" << endl;
				return true;
			}

		}

		return false;
	}

	void EraseWorkerHistory() {
		for (int i = 0; i < companyCount; i++)
			ptr[i].ResetWorkerHistory();
	}

	void EraseMasterWorkerHistory() {
		for (int i = 0; i < companyCount; i++)
			ptr[i].ResetMasterWorkerHistory();
	}
};

//Random number generator
int randomNumber(int limiter) {

	int answer;

	while (true) {

		answer = rand() % limiter;

		if (answer < limiter)
			return answer;
	}

}

//Table Output Function
void CompanyConsoleOutput(CompanyList companies, int rotationTurn) {

	string mw = " ", w = " ";
	int count = 0;
	cout << "ROTATION: " << to_string(rotationTurn) << endl;

	for (int i = 0; i < companyCount; i++) {
		mw = " ";
		w = " ";
		count = 0;

		cout << left << setw(10) << "Firma Adi" << "|" << left << setw(20) << "Kidemli Isci Adi" << endl;

		//Company Name
		cout << left << setw(10) << companies.ptr[i].name << "|";

		//Master Workers
		for (string masterWorker : companies.ptr[i].masterWorkerHistory)
			mw += masterWorker + " ";
		cout << left << setw(20) << mw << endl;

		//Workers
		cout << "Isciler:" << endl;
		for (string worker : companies.ptr[i].workerHistory) {

			cout << left << setw(4) << worker << " ";
			count++;
			if (count == companies.ptr[i].workerCount) {
				cout << endl;
				count = 0;
			}
		}

		if (i < 8)
			cout << "\n------------------------------------------------------------ \n";
		else
			cout << endl << "************************************************************" << endl;
	}
}

int main() {

	srand(time(NULL));

	WorkerList workers;
	MasterWorkerList masters;
	CompanyList companies;

	vector<int> workerBans;
	vector<int> masterWorkerBans;

	int rotationCount = 1;

	//Assigning Master Workers;
	for (int i = 0; i < companyCount; i++) {
		int masterCount = companies.ptr[i].masterWorkerCount;
		string companyName = companies.ptr[i].name;

		//listing avaible master workers
		vector<int>arr = masters.FreeGuys(companies.ptr[i].name, rotationCount);

		if (arr.empty())
			continue;

		for (int k = 0; k < masterCount; k++) {

			if (arr.size() == 0)
				break;

			//removing assigned master worker
			int temp = randomNumber(arr.size());
			int  it = arr[temp];
			arr.erase(arr.begin() + temp);

			//Saving history
			companies.ptr[i].getMasterWorker(masters.ptr[it].name);
			masters.ptr[it].Transfer(companyName);
		}
	}

	while (rotationCount <= 20) {

		//Maximum Rotaiton condition check
		if (companies.RotationDone()) {

			//Erasing Workers' history and Companies' Master Workers & Workers history
			companies.EraseMasterWorkerHistory();
			companies.EraseWorkerHistory();
			workers.EraseHistory();

			//Assigning Master Workers;
			for (int i = 0; i < companyCount; i++) {
				int masterCount = companies.ptr[i].masterWorkerCount;
				string companyName = companies.ptr[i].name;

				//listing avaible master workers
				vector<int>arr = masters.FreeGuys(companies.ptr[i].name, rotationCount);

				if (arr.empty())
					continue;

				for (int k = 0; k < masterCount; k++) {

					if (arr.size() == 0)
						break;

					int temp = randomNumber(arr.size());
					int  it = arr[temp];
					arr.erase(arr.begin() + temp);

					companies.ptr[i].getMasterWorker(masters.ptr[it].name);
					masters.ptr[it].Transfer(companyName);
				}
			}
		}

		//Assigning Workers
		for (int i = 0; i < companyCount; i++) {

			string companyName = companies.ptr[i].name;
			int wCount = companies.ptr[i].workerCount;

			//listing avaible workers
			vector<int>arr = workers.FreeGuys(companies.ptr[i].name, rotationCount);

			if (arr.empty())
				continue;

			for (int k = 0; k < wCount; k++) {

				if (arr.size() == 0)
					break;

				int temp = randomNumber(arr.size());
				int  it = arr[temp];

				arr.erase(arr.begin() + temp);

				companies.ptr[i].getWorker(workers.ptr[it].name);
				workers.ptr[it].Transfer(companyName);
			}
		}

		//Output the table
		CompanyConsoleOutput(companies, rotationCount);
		rotationCount++;
	}

	return 0;
}
