CC = g++
CFLAGS = -Wall
OBJ = ACME

all: $(OBJ)

$(OBJ): main.cpp
	@$(CC) -std=c++11 main.cpp -o $(OBJ) 	

clean:
	@rm -rf *.exe
	@rm -rf ACME
