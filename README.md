# ACME


## Çalışma Prensibi

###Sınıflar

"ACME" projesi kendi içinde 6 tane sınıf bulundurmaktadır. Bu sınıflardan 3 tanesi ana sınıflar olan "Worker", "MasterWorker" ve "Company" sınıflarıdır. Ana sınıfların yanında ki 3 sınıf yardımcı list sınıflarıdır. Ayrıca yardımcı sınıflar listelemeyi kendi içinde işçi, kıdemli işçi ve firma değelerini arrayler içinde bulundurmaktadır.

Ana sınıflar değerler hakkında bilgi bulundururlar ve kendi içlerinde olan fonksiyonlarla sınıfların kendilerini kontrollerini sağlarlar. List sınıfları ise genel etkissi olan yada kontrol amaçlı olan metodlar bulundururlar.

###Rastgele Sayı

Program içinde gerekli olan rastgele işçilerin firmalara ayarlanmaları "randomNumber" fonksiyonu ile karşılanmaktadır, ama bu fonksiyona gönderilmeden işçi liste sınıflarında bulunan "FreeGuy" fonksiyonundan bir vector oluşturulur, ardından "randomNumber" fonksiyonuna yollanır. "FreeGuy" fonksiyonu işçi grupları içinde, işçilerin seçilen şirkete önceden gidip-gitmediğini yada o rotasyonda başka şirkete gitmemesini kontrol eder ve bu hususlara uygun olanları vector içerisine eklenir. Bu sayede hem sadece seçilmesi gereken kişileri bulmuş olur hemde süreden yarar sağlanır.

###RotationDone

"CompanyList" sınıfı aynı zamanda kendi içinde "RotationDone" adında bir fonksiyonda bulundurur. Bu fonksiyon o liste sınıfı içerisinde şirketleri giden kişilerin maksimum rotasyonu yapıp yapmadığını kontrol. Fonksiyon uygun duruma göre bir bool cevabı gönderip fonksiyon içerisinde kıdemli değişimi gibi olaylara etki eder ve aynı zamanda "MAKSİMUM ROTASYON DOLDU" cümlesini çıktı verir.



